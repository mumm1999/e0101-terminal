from pathlib import Path
from filecmp import dircmp


def test_remove_data_wrong_txt():
    """
    检查 data/wrong.txt 文件已被删除
    """
    p = Path("data/wrong.txt")
    assert not p.exists()


def test_removed_folder_del():
    """
    检查 folder_del 文件夹及其内容已被全部删除
    """
    p = Path("folder_del")
    assert not p.exists()


def test_copied_data_plain_txt_to_script_data_txt():
    """
    检查 data/plain.txt 文件已被复制为 script/data.txt, 两者内容相同
    """
    f1 = Path("data/plain.txt")
    assert f1.exists()

    f2 = Path("script/plain.txt")
    assert f2.exists()

    assert f1.read_text() == f2.read_text()


def test_copied_data_to_data_bak():
    """
    检查 data 文件夹及其内容已被全部复制为 data_bak
    """
    dcmp = dircmp("data", "data_bak")
    assert list(dcmp.left_only) == ["table.tsv.bak"]  # 任务6、7
    assert list(dcmp.right_only) == ["table.csv", "table.tsv"]  # 任务6、7


def test_created_data_new_tables_folder():
    """
    检查已创建了 data_new/tables 文件夹
    """
    p = Path("data_new/tables")
    assert p.exists() and p.is_dir()


def test_moved_data_table_csv_to_data_new_tables():
    """
    检查已将 data/table.csv 移动至 data_new/tables/ 下
    """
    f_old = Path("data/table.csv")
    assert not f_old.exists()

    f_new = Path("data_new/tables/table.csv")
    assert f_new.exists() and f_new.is_file()


def test_renamed_data_table_tsv_to_data_table_tsv_bak():
    """
    检查已将 data/table.tsv 重命名为 data/table.tsv.bak
    """
    f_old = Path("data/table.tsv")
    assert not f_old.exists()

    f_new = Path("data/table.tsv.bak")
    assert f_new.exists() and f_new.is_file()
