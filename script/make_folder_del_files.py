import shutil
from pathlib import Path


base = Path("folder_del")
shutil.rmtree(base, ignore_errors=True)
base.mkdir()
# 删除后重建 folder_del 空目录

for i in range(200):
    f = base / f"data{i:03d}.txt"
    f.write_text("Garbage data...")
# 创建 200 个纯文本文件
